    // sidebar

    // Close menu
    $("#close-menu").click(function(e) {
      e.preventDefault();
      $("#sidebar-wrapper").toggleClass("active");
  });
  // Open menu
  $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#sidebar-wrapper").toggleClass("active");
  });


// form-validation
$(document).ready(function() {
  $("#basic-form").validate({
    rules: {
      name : {
        required: true,
        minlength: 3
      },
      barcode: {
        required: true,
        minlength: 6
      },
      email: {
        required: true,
        email: true
      },
      stock: {
        required: true,
        number:true,
      },
      description: {
        required: true,
      },
      cost : {
        required: true,
        minlength: 4
      },
      sell : {
        required: true,
        minlength: 4
      },
      margin : {
        required: true,
      },
      ppn : {
        required: true,
      },
    },
    
    messages : {
      name: {
        minlength: "Name should be at least 3 characters"
      },
      barcode: {
        minlength: "please enter 6 number"
      },
      stock: {
        number: "Please enter just a number"
      },
      cost: {
        minlength: "Please input at least 1000 Rupiah"
      },
      sell: {
        minlength: "Please input at least 1000 Rupiah"
      },
      margin: {
        required: "You Must insert the field"
      },
      ppn: {
        required: "You Must insert the field"
      },
     
    }
  });
});

// image-upload
function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $('#blah').attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]);
    }
  }
  
  $("#imgInp").change(function() {
    readURL(this);
  });



// Form Detail


$('.btn').on('click', function () {
  var nama = $('#basic-form').find('input[name="name"]').val();
  var barcode = $('#basic-form').find('input[name="barcode"]').val();
  var category = $('#basic-form').find('select[name="category"]').val();
  var stock = $('#basic-form').find('input[name="stock"]').val();
  var cost = $('#basic-form').find('input[name="cost"]').val();
  var sell = $('#basic-form').find('input[name="sell"]').val();
  var description = $('#basic-form').find('input[name="description"]').val();
  var margin = $('#basic-form').find('input[name="margin"]').val();
  var ppn = $('#basic-form').find('input[name="ppn"]').val();

  $('.modal-body').html(`

          
                  <p> Nama =`  + (nama) + `</p>
                  <p>Barcode =` + (barcode) + `</p>
                  <p>Category =` + (category) + `</p>
                  <p>Stock ` + (stock) + `</p>
                  <p>Cost Price=` + (cost) + ` & Sell Price= `+ (sell) +`</p>
                  <p>Description =` + (description) + `</p>
                  <p>Margin =` + (margin) + `</p>
                  <p>PPN =` + (ppn) + `</p>
     
  `);

});




